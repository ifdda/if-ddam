      subroutine fonctiongreensurffftscal(nx,ny,ntp,nx2,ny2,nxm,nym,n1m
     $     ,nplan,nplanm,nmatim,nbs,ntotalm,aretecube,a,matind
     $     ,matindplan ,matindice ,matrange,b11,a11,uinc,planb)
      implicit none
      integer i,j,nn,ll,jj,ii,kk,indice,nx,ny,ntp,nx2,ny2,nxm,nym,n1m
     $     ,nplan,nplanm,nmatim ,nbs,ntotalm,n1, FFTW_BACKWARD
      integer matindice(nplanm,nmatim) ,matind(0:2*n1m*n1m)
     $     ,matindplan(nplan,nplan)
      double complex matrange(nbs,5),Ixx,Ixy,Ixz,Izx,Izz,Sxx,Sxy,Sxz,Syy
     $     ,Szx,Syz,Szy
      double complex a11(2*nxm,2*nym ,nplanm),b11(ntotalm),uinc(3)
      double precision sphi,cphi,s2phi,c2phi,aretecube,a(0:2*n1m*n1m)
      integer*8 planb
      FFTW_BACKWARD=+1

      
c     initialise la matrice FFT
      do nn=1,ntp
         do ll=nn,ntp

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(ii,jj,indice,i,j,n1,kk)
!$OMP& PRIVATE(Ixx,Ixy,Ixz,Izz,Izx,sphi,cphi,s2phi,c2phi)
!$OMP& PRIVATE(Sxx,Sxy,Sxz,Syy,Syz,Szx,Szy)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)            
            do jj=1,ny2
               do ii=1,nx2
                  indice=ii+nx2*(jj-1)
                  if (ii.eq.nx+1.or.jj.eq.ny+1) then
                     b11(indice)=0.d0                 
                  else
                     if (ii.gt.nx) then
                        i=(ii-1)-nx2
                     else
                        i=ii-1
                     endif                    
                     if (jj.gt.ny) then
                        j=(jj-1)-ny2
                     else
                        j=jj-1
                     endif
                     n1=i*i+j*j
                     kk=matindice(matindplan(ll,nn),matind(n1))
                     Ixx=matrange(kk,1)
                     Ixy=matrange(kk,2)
                     Ixz=matrange(kk,3)
                     Izz=matrange(kk,4)
                     Izx=matrange(kk,5)
                     sphi=aretecube*dble(i)/a(n1)
                     cphi=aretecube*dble(j)/a(n1)
                     s2phi=2.d0*sphi*cphi
                     c2phi=cphi*cphi-sphi*sphi
                     Sxx=Ixx+c2phi*Ixy
                     Sxy=-s2phi*Ixy
                     Sxz=sphi*Ixz
                     Syy=Ixx-c2phi*Ixy
                     Syz=cphi*Ixz
                     Szx=sphi*Izx
                     Szy=cphi*Izx
               
c*******************************************************
                     b11(indice)=dconjg(uinc(1))*(Sxx*uinc(1)+Sxy*uinc(2
     $                    )+Sxz*uinc(3))+dconjg(uinc(2))*(Sxy*uinc(1)
     $                    +Syy*uinc(2)+Syz*uinc(3))+dconjg(uinc(3))*(Szx
     $                    *uinc(1)+Szy*uinc(2)+Izz*uinc(3))
                  endif
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL              

#ifdef USE_FFTW
            call dfftw_execute_dft(planb,b11,b11)       
#else
            call fftsingletonz2d(b11,NX2,NY2,FFTW_BACKWARD)
#endif


            kk=matindplan(ll,nn)
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(ii,jj,indice)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)             
            do jj=1,ny2
               do ii=1,nx2
                  indice=ii+nx2*(jj-1)
                  a11(ii,jj,kk)=b11(indice)
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL            
         enddo
      enddo


      end

