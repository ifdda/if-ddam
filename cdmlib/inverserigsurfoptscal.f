      subroutine inverserigsurfoptscal(xi,xr,FFlocscal,nbsphere ,ndipole
     $     ,nx,ny,nz ,nx2,ny2,nxm,nym,nzm,nplanm,ntotalm,nmax
     $     ,matindplan,b31 ,FF ,FF0,FFloc,b11,a11 ,WRK,nlar ,ldabi
     $     ,polarisa,methodeit,tol ,tol1,nloop ,ncompte,nlim,Uinc,planf
     $     ,planb ,tempsreelmvp,nstop ,infostr)

      implicit none
      integer nbsphere,ndipole,nx,ny,nz,nx2,ny2 ,nxm,nym,nzm,nplanm
     $     ,ntotalm,nmax,nlar,ldabi,nloop,ncompte,nstop
      integer matindplan(nzm,nzm)
      
      double precision tol,tol1

      double complex, dimension(3*nxm*nym*nzm) :: xr,xi
      double complex, dimension(3*nxm*nym*nzm,12) :: wrk
      double complex, dimension(3*nxm*nym*nzm) :: FF,FF0,FFloc
      double complex a11(2*nxm,2*nym,nplanm),b11(4*nxm*nym),b31(4 *nxm
     $     *nym),Uinc(3)
      double complex, dimension(nxm*nym*nzm,3,3) :: polarisa
      double complex, dimension(nxm*nym*nzm) :: FFlocscal

      character(64) infostr,message
      character(12) methodeit
c     ********************
      integer i,nlim,ndim,nou,nstat,steperr,nt,ii
      double precision NORM,t0,t1,t2,tole,tempsreelmvp
      double complex ALPHA,BETA,GPETA,DZETA,R0RN,QMR1,QMR2,QMR3,QMR4
     $     ,QMR5,QMR6,QMR7,QMR8,QMR9
      character(8)  :: date
      character(10) :: time
      character(5)  :: zone
      integer values(8),values2(8)
      double complex DOTS(4)
      integer *8 planb,planf
      
      ncompte=0
      nou=0
      ndim=nbsphere
      nloop=0
    
      write(*,*) 'tolerance',tol,tol1

      call cpu_time(t1)
      call date_and_time(date,time,zone,values)



      
      if (methodeit(1:7).eq.'GPBICG1') then

 2002    call GPBICG(XI,XR,FF0,ldabi,ndim,nlar,nou,WRK,NLOOP,Nlim,TOL
     $        ,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR)
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         write(*,*) 'ncompte',ncompte,nstat
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL           
         endif

         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)

         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         
         if (nstat.ne.1) goto  2002
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM   
         nloop=nloop+1

      elseif (methodeit(1:7).eq.'GPBICG2') then
         write(*,*) 'methodeit',methodeit
 2009    call GPBICG2(XI,XR,FF0,ldabi,ndim,nlar,nou,WRK,NLOOP,Nlim
     $        ,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR) 
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1           
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL  
         endif
         
        call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)
         
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         
         if (nstat.ne.1) goto  2009
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
         nloop=nloop+1
      elseif (methodeit(1:10).eq.'GPBICGplus') then
         write(*,*) 'methodeit',methodeit
 2016    call GPBICGplus(XI,XR,FF0,ldabi,ndim,nlar,nou,WRK,NLOOP,Nlim
     $        ,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR) 

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1           
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL  
         endif
         
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)
         
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         
         if (nstat.ne.1) goto  2016
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM   
         nloop=nloop+1
         
      elseif (methodeit(1:10).eq.'GPBICGsafe') then
         write(*,*) 'methodeit',methodeit
 2019    call GPBICGsafe(XI,XR,FF0,ldabi,ndim,nlar,nou,WRK,NLOOP,Nlim
     $        ,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR) 

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1           
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL  
         endif
         
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)
         
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         
         if (nstat.ne.1) goto  2019
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM   
         nloop=nloop+1
         
      elseif (methodeit(1:8).eq.'GPBICGAR') then
         write(*,*) 'methodeit',methodeit
 2010    call GPBICGAR(XI,XR,FF0,ldabi,ndim,nlar,nou,WRK,NLOOP,Nlim
     $        ,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR) 
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL   
         endif
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)
         
         
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         
         if (nstat.ne.1) goto  2010
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
         nloop=nloop+1
      elseif (methodeit(1:9).eq.'GPBICGAR2') then
         write(*,*) 'methodeit',methodeit
 2011    call GPBICGAR2(XI,XR,FF0,ldabi,ndim,nlar,nou,WRK,NLOOP,Nlim
     $        ,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR) 
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL    
         endif
         
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         
         if (nstat.ne.1) goto  2011
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
         nloop=nloop+1
      elseif (methodeit(1:12).eq.'BICGSTARPLUS') then
         write(*,*) 'methodeit',methodeit
 2015    call GPBICGSTARPLUS(XI,XR,FF0,ldabi,ndim,nlar,nou,WRK,NLOOP
     $        ,Nlim,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT
     $        ,STEPERR) 
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL    
         endif
         
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         
         if (nstat.ne.1) goto  2015
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
         
      elseif (methodeit(1:7).eq.'GPBICOR') then
         write(*,*) 'methodeit',methodeit   
 2020    call GPBICOR(XI,XR,FF0,FFloc,ldabi,ndim,nlar,nou,WRK ,NLOOP
     $        ,Nlim,TOL,NORM,ALPHA,BETA,GPETA,DZETA,QMR1 ,QMR2,NSTAT
     $        ,STEPERR)

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         ncompte=ncompte+1
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL    
         endif
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         if (nstat.ne.1) goto  2020
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
         nloop=nloop+1
         
      elseif (methodeit(1:4).eq.'CORS') then
         write(*,*) 'methodeit',methodeit   
 2021    call CORS(XI,XR,FF0,ldabi,ndim,nlar,nou,WRK ,NLOOP ,Nlim,TOL
     $        ,NORM,ALPHA,GPETA,DZETA,BETA,NSTAT ,STEPERR)

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         ncompte=ncompte+1
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL    
         endif
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         if (nstat.ne.1) goto  2021
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM            
         
      elseif (methodeit(1:6).eq.'QMRCLA') then
c$$$         write(*,*) 'methodeit',methodeit            
c$$$ 2003    call PIMZQMR(FFloc,XI,XR,FF0,WRK,NORM,LDABI,NDIM,NLAR,QMR1
c$$$     $        ,QMR2,QMR3,QMR4,QMR5,QMR6,QMR7,QMR8,QMR9,DOTS,NOU,NT
c$$$     $        ,nloop,NLIM,TOLE ,TOL ,NSTAT ,STEPERR)
c$$$         if (nstat.lt.0) then
c$$$            nstop=1
c$$$            infostr='Problem to solve Ax=b'
c$$$            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
c$$$            return
c$$$         endif
c$$$         
c$$$         if (nstop == -1) then
c$$$            infostr = 'Calculation cancelled during iterative method'
c$$$            return
c$$$         endif
c$$$         
c$$$         ncompte=ncompte+1
c$$$         if (nstat.eq.1) then
c$$$            do i=1,nbsphere
c$$$               xi(i)=FFloc(i)
c$$$            enddo
c$$$            nt=1
c$$$         endif
c$$$c     write(*,*) 'ncompte',ncompte,nt,tole
c$$$         if (nt.eq.1) then
c$$$            call produitfftmatvectsurmopt(xi,xr,nbsphere,ndipole,nx,ny
c$$$     $           ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
c$$$     $           ,matindplan,b31,b32,b33,FF,b11,b12,b13,a11 ,a12 ,a13
c$$$     $           ,a22,a23,a31,a32 ,a33,polarisa,planb,planf)
c$$$            
c$$$         elseif (nt.eq.2) then
c$$$c     calcul avec le transpose                   
c$$$            call produitfftmatvectsurmopttrans(xi,xr,nbsphere,ndipole,nx
c$$$     $           ,ny,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
c$$$     $           ,matindplan,b31,b32,b33,FF,b11,b12,b13,a11 ,a12 ,a13
c$$$     $           ,a22,a23,a31,a32 ,a33,polarisa,planb,planf)
c$$$         endif
c$$$         
c$$$         if (nstat.ne.1) goto  2003
c$$$c     compute the Residue
c$$$         tol1=0.d0
c$$$!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
c$$$!$OMP DO  REDUCTION(+:tol1)           
c$$$         do i=1,nbsphere
c$$$            xr(i)=xr(i)-FF0(i)            
c$$$            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
c$$$         enddo            
c$$$!$OMP ENDDO 
c$$$!$OMP END PARALLEL
c$$$         tol1=dsqrt(tol1)/NORM  

         
      elseif (methodeit(1:5).eq.'TFQMR') then
         write(*,*) 'methodeit',methodeit
 2004    call TFQMR(FFloc,Xi,XR,FF0,ldabi,ndim,nlar,nou,WRK,nloop
     $        ,NLIM,TOL,NORM,QMR1,QMR2,QMR3,QMR4,QMR5,QMR6,NSTAT
     $        ,STEPERR)
         
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO           
            do i=1,nbsphere
               xi(i)=FFloc(i)
               FFlocscal(i)=FFloc(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL  
         endif
         write(*,*) 'ncompte',ncompte,nou
         
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)

         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2004

         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL          
         tol1=dsqrt(tol1)/NORM
         nloop=nloop+1
         
      elseif (methodeit(1:5).eq.'CG') then
         write(*,*) 'methodeit',methodeit
 2005    call ZCG(XI,XR,FF0,NORM,WRK,QMR1,QMR2,QMR3,LDABI,NDIM,NLAR
     $        ,NOU,NSTAT,NLOOP,NLIM,TOLE,TOL)
         
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL   
         endif
         write(*,*) 'ncompte',ncompte,nou
         
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)
         
         if (nstat.ne.1) goto  2005
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM   
         
      elseif (methodeit(1:8).eq.'BICGSTAB') then
         write(*,*) 'methodeit',methodeit
 2006    call PIMZBICGSTAB(FFLOC,Xi,XR,FF0,ldabi,nlar,ndim,nou,WRK
     $        ,QMR1,QMR2,QMR3,NORM,TOL,nloop,nlim,NSTAT,STEPERR)
         
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO           
            do i=1,nbsphere
               xi(i)=FFloc(i)
               FFlocscal(i)=FFloc(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL     
         endif
         write(*,*) 'ncompte',ncompte,nloop,nou
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)

         if (nstat.ne.1) goto  2006
         
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            FF(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(FF(i)*dconjg(FF(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM   
         
      elseif (methodeit(1:12).eq.'QMRBICGSTAB1') then
         write(*,*) 'methodeit',methodeit
         nt=1
 2007    call QMRBICGSTAB(FFloc,Xi,XR,FF0,ldabi,ndim,nlar,nou,WRK
     $        ,nloop,nlim,TOL,TOLE,NORM,QMR1,QMR2,QMR3,QMR4,QMR5
     $        ,QMR6,QMR7,QMR8,QMR9,NT,NSTAT ,STEPERR)
         
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
         endif
         write(*,*) 'ncompte',ncompte,nou
         
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)
         
         if (nstat.ne.1) goto  2007
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM   
      elseif (methodeit(1:12).eq.'QMRBICGSTAB2') then
         write(*,*) 'methodeit',methodeit
         nt=2
 2008    call QMRBICGSTAB(FFloc,Xi,XR,FF0,ldabi,ndim,nlar,nou,WRK
     $        ,nloop,nlim,TOL,TOLE,NORM,QMR1,QMR2,QMR3,QMR4,QMR5
     $        ,QMR6,QMR7,QMR8,QMR9,NT,NSTAT ,STEPERR)
         
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO           
            do i=1,nbsphere
               FFlocscal(i)=xi(i)       
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL  
         endif
         write(*,*) 'ncompte',ncompte,nou
         call produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx,ny
     $        ,nz,nx2,ny2,nxm,nym,nzm,nzm,nplanm,ntotalm,nmax
     $        ,matindplan,b31,FF,b11,a11,polarisa,planb,planf)

         if (nstat.ne.1) goto  2008
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO  REDUCTION(+:tol1)           
         do i=1,nbsphere
            xr(i)=xr(i)-FF0(i)            
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM   
      else
         write(*,*) 'Iterative method not correct'
         nstop=1
         infostr='Iterative method not correct'
         return         
      endif



c     repasse en vectoriel
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,ii) 
!$OMP DO
      do i=1,nbsphere
         ii=3*(i-1)
         FFloc(ii+1)=FFlocscal(i)*uinc(1)
         FFloc(ii+2)=FFlocscal(i)*uinc(2)
         FFloc(ii+3)=FFlocscal(i)*uinc(3)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      
      call cpu_time(t2)
      call date_and_time(date,time,zone,values2)
      message='to solve Ax=b'
      call calculatedate(values2,values,t2,t1,message,tempsreelmvp)

      write(*,*) 'Method iterative used             : ',methodeit
      write(*,*) 'Tolerance obtained                : ',tol1
      write(*,*) 'Tolerance asked                   : ',tol
      write(*,*) 'Number of product Ax needs        : ',ncompte

      end
