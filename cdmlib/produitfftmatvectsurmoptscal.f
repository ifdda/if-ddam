c     calcul pour une surface le produit: (I-A alpha)xi=xr avec FFT
  
      subroutine produitfftmatvectsurmoptscal(xi,xr,nbsphere,ndipole,nx
     $     ,ny,ntp,nx2,ny2,nxm,nym,nzm,nplan,nplanm,ntotalm,nmax
     $     ,matindplan,x1,FF,b11,a11,polarisa,planb,planf)
      implicit none
      integer i,j,ii,jj,nn,indice,ll,kk
      double complex x1(ntotalm),b11(ntotalm)
      integer nbsphere,ndipole,nx,ny,ntp,nx2,ny2,nxm,nym,nzm,nplanm
     $     ,nplan,ntotalm,nmax,matindplan(nplan,nplan)
      double complex xi(3*nmax),xr(3*nmax),FF(3*nmax),a11(2*nxm,2*nym
     $     ,nplanm),polarisa(nmax,3,3)
      integer FFTW_FORWARD,FFTW_BACKWARD,FFTW_ESTIMATE,nxy
      integer*8 planf,planb
      double precision dntotal

      FFTW_FORWARD=-1
      FFTW_BACKWARD=+1
      FFTW_ESTIMATE=64
      nxy=nx2*ny2
      dntotal=dble(nxy)
      
      
c      write(*,*) 'aa',a11,a12,a13,a22,a23,a31,a32,a33
c     calcul l'identite I xi
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC)
      do i=1,nbsphere
         xr(i)=xi(i)
c         write(*,*) 'xrrr',xr(i),i
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC)
      do i=1,ndipole
         FF(i)=0.d0
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      
c     calcul FFT du vecteur B
      do nn=1,ntp
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC)
         do i=1,nx2*ny2
            x1(i)=0.d0
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL 

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indice,ii,jj)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)       
         do j=1,ny
            do i=1,nx
               indice=i+nx2*(j-1)                  
               ii=i+nx*((j-1)+ny*(nn-1))             
               x1(indice)=polarisa(ii,1,1)*xi(ii)
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL 

#ifdef USE_FFTW
         call dfftw_execute_dft(planb,x1,x1)
#else
         call fftsingletonz2d(x1,NX2,NY2,FFTW_BACKWARD)
#endif


         do ll=1,ntp
            kk=matindplan(ll,nn)
            if (ll.ge.nn) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indice)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2) 
               do j=1,ny2
                  do i=1,nx2
                     indice=i+nx2*(j-1)       
                     b11(indice)=a11(i,j,kk)*x1(indice)
                  enddo
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
            else
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indice) 
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)          
               do j=1,ny2
                  do i=1,nx2
                     indice=i+nx2*(j-1)      
                     b11(indice)=a11(i,j,kk)*x1(indice)
                  enddo
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
            endif

#ifdef USE_FFTW
            call dfftw_execute_dft(planf,b11,b11)
#else
            call fftsingletonz2d(b11,NX2,NY2,FFTW_FORWARD)
#endif

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indice,ii) 
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)     
            do j=1,ny
               do i=1,nx
                  indice=i+nx2*(j-1)           
                  ii=i+nx*((j-1)+ny*(ll-1))
                  FF(ii)=FF(ii)+b11(indice)
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
         enddo
      enddo

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)    
!$OMP DO SCHEDULE(STATIC)
      do i=1,ndipole
         xr(i)=xr(i)-FF(i)/dntotal
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
      return
      end
