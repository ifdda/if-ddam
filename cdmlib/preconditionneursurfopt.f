c     cette routine multiplie un vecteur par la matrice inverse estimée
c     de la matrice toeplitz initialie.

      subroutine preconditionneursurfopt(FF,xi,xr,nx,ny ,nz ,nbsphere
     $     ,ndipole,nxm,nym ,nzm ,sdetnn ,ipvtnn ,vectxx ,vectyy,vectzz
     $     ,plan2b,plan2f ,infostr ,nstop)
      implicit none
      integer nx,ny,nz,nbsphere,nstop,nxm,nym,nzm,ntest,nz3,ndipole,ic
     $     ,il
      double precision x,y,z,x0,y0,z0,tmp,tmp1,tmp2
      double complex FF(3*nxm*nym*nzm),xi(3 *nxm*nym*nzm),xr(3*nxm*nym
     $     *nzm),propaesplibre(3,3)

      character(64) infostr

      integer i,j,k,ii,jj,kk,ix,iy,iz,i2,j2,k2,kkk,l,ll,k1,j1
      double precision dn1x,dn2x,dn1y,dn2y
      double complex polamoy,dn11c,dn12c,dn21c,dn22c
      double complex sdetnn(3*nzm,3*nzm ,nxm*nym)

      integer n1,n2,n3
      double precision dnxy,normefrobenius

      integer job,lda,info,lwork,NRHS
      double complex  deterz(2)
      
      integer nmaxcompo
      double complex vectxx(nx*ny),vectyy(nx*ny),vectzz(nx*ny)
      double complex , dimension (:,:), allocatable :: sdettmp
      double complex , dimension (:), allocatable :: Txx,Txy,Txz ,Tyy
     $     ,Tyz ,Tzx, Tzy, Tzz,chanxx,chanxy,chanxz,chanyy,chanyz,chanzx
     $     , chanzy, chanzz,worktmp ,work
      integer , dimension (:),allocatable ::ipvttmp
      integer jx,jy,ipvtnn(nzm*3,nxm*nym)
      double complex , dimension (:,:), allocatable :: spola

      integer FFTW_FORWARD,FFTW_ESTIMATE,FFTW_BACKWARD
      integer*8 plan2b,plan2f
      integer valuesf(8),valuesi(8)
      double precision tf,ti
      character(64) message
      character(8)  :: date
      character(10) :: time
      character(5)  :: zone
      
      FFTW_FORWARD=-1
      FFTW_BACKWARD=+1
      FFTW_ESTIMATE=64
      nz3=nz*3
 
      do i=1,nz
c     sauvegarde dans un petit vecteur

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k,kk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
         do j=1,ny
            do k=1,nx
               kk=k+nx*(j-1)+nx*ny*(i-1)
               vectxx(k+nx*(j-1))=FF(3*(kk-1)+1)
               vectyy(k+nx*(j-1))=FF(3*(kk-1)+2)
               vectzz(k+nx*(j-1))=FF(3*(kk-1)+3)
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL

#ifdef USE_FFTW
         call dfftw_execute_dft(plan2b,vectxx,vectxx)
         call dfftw_execute_dft(plan2b,vectyy,vectyy)
         call dfftw_execute_dft(plan2b,vectzz,vectzz)
#else 
         call fftsingletonz2d(vectxx,nx,ny,FFTW_BACKWARD)
         call fftsingletonz2d(vectyy,nx,ny,FFTW_BACKWARD)
         call fftsingletonz2d(vectzz,nx,ny,FFTW_BACKWARD)         
#endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k,kk,kkk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
         do j=1,ny
            do k=1,nx
               kk=k+nx*(j-1)
               kkk=i+nz*(j-1)+nz*ny*(k-1)
               xi(3*(kkk-1)+1)=vectxx(kk)
               xi(3*(kkk-1)+2)=vectyy(kk)
               xi(3*(kkk-1)+3)=vectzz(kk)
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      enddo

      lda=nz3
      NRHS=1
c     ***********************************************
c     fin calcul vecteur 
c     ***********************************************
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(l,i,j,k,kk,ii,jj)
!$OMP DO SCHEDULE(STATIC) 
      do l=1,nx*ny
         call  zgetrs( 'N',nz3, NRHS, Sdetnn(1:nz3,1:nz3,l), LDA,
     $        IPVtnn(1:nz3,l),xi((l-1)*nz3+1:l*nz3),LDA,INFO)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         

c     ****************************************
c     refait les opérations inverses
c     ****************************************
      dnxy=dble(nx*ny)
      do i=1,nz
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k,kk,kkk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
         do j=1,ny
            do k=1,nx
               kk=k+nx*(j-1)
               kkk=i+nz*(j-1)+nz*ny*(k-1)
               vectxx(kk)=xi(3*(kkk-1)+1)
               vectyy(kk)=xi(3*(kkk-1)+2)
               vectzz(kk)=xi(3*(kkk-1)+3)
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL

#ifdef USE_FFTW
         call dfftw_execute_dft(plan2f,vectxx,vectxx)
         call dfftw_execute_dft(plan2f,vectyy,vectyy)
         call dfftw_execute_dft(plan2f,vectzz,vectzz)
#else 
         call fftsingletonz2d(vectxx,nx,ny,FFTW_FORWARD)
         call fftsingletonz2d(vectyy,nx,ny,FFTW_FORWARD)
         call fftsingletonz2d(vectzz,nx,ny,FFTW_FORWARD)         
#endif
            
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k,kk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)      
         do j=1,ny
            do k=1,nx
               kk=k+nx*(j-1)+nx*ny*(i-1)
               FF(3*(kk-1)+1)=vectxx(k+nx*(j-1))/dnxy
               FF(3*(kk-1)+2)=vectyy(k+nx*(j-1))/dnxy
               FF(3*(kk-1)+3)=vectzz(k+nx*(j-1))/dnxy
            enddo
         enddo   
!$OMP ENDDO 
!$OMP END PARALLEL
                     
      enddo

c     call cpu_time(tf)
c     call date_and_time(date,time,zone,valuesf)      
c     call calculatedate(valuesf,valuesi,tf,ti,message)
         
      end
