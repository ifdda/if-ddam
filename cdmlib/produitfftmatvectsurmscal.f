c     calcul pour une surface le produit: (I-A alpha)xi=xr avec FFT
  
      subroutine produitfftmatvectsurmscal(xi,xr,nbsphere,ndipole,nx,ny
     $     ,ntp,nx2,ny2,nxm,nym,nzm,nplan,nplanm,ntotalm,nmax,matindplan
     $     ,Tabdip,x1,FF,b11,a11,polarisa,planb,planf)
      implicit none
      integer i,j,m,ii,jj,nn,indice,ll,kk
      integer nbsphere,ndipole,nx,ny,ntp,nx2,ny2,nxm,nym,nzm,nplanm
     $     ,nplan,ntotalm,nmax,matindplan(nplan,nplan),Tabdip(nxm*nym
     $     *nzm)
      double complex xi(3*nmax),xr(3*nmax),FF(3*nmax),a11(2*nxm,2*nym
     $     ,nplanm),polarisa(nmax,3,3),x1(ntotalm),b11(ntotalm)
      integer*8 planf,planb
      integer FFTW_FORWARD,FFTW_BACKWARD,FFTW_ESTIMATE,nxy
      double precision dntotal

      FFTW_FORWARD=-1
      FFTW_BACKWARD=+1
      FFTW_ESTIMATE=64
      nxy=nx2*ny2
      dntotal=dble(nxy)
      
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC)
      do i=1,nbsphere
         xr(i)=xi(i)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC)
      do i=1,ndipole
         FF(i)=0.d0
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      
c     calcul FFT du vecteur B
      do nn=1,ntp
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC)
         do i=1,nx2*ny2
            x1(i)=0.d0
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL 

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indice,ii,jj,m)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)       
         do j=1,ny
            do i=1,nx
               indice=i+nx2*(j-1)                  
               ii=i+nx*((j-1)+ny*(nn-1))
               if (Tabdip(ii).ne.0) then
                  m=Tabdip(ii)           
                  x1(indice)=polarisa(m,1,1)*xi(m)
               endif                       
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL 

#ifdef USE_FFTW
         call dfftw_execute_dft(planb,x1,x1)
#else
         call fftsingletonz2d(x1,NX2,NY2,FFTW_BACKWARD)
#endif

         do ll=1,ntp
            kk=matindplan(ll,nn)
            if (ll.ge.nn) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indice)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2) 
               do j=1,ny2
                  do i=1,nx2
                     indice=i+nx2*(j-1)       
                     b11(indice)=a11(i,j,kk)*x1(indice)
                  enddo
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
            else
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indice) 
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)          
               do j=1,ny2
                  do i=1,nx2
                     indice=i+nx2*(j-1)      
                     b11(indice)=a11(i,j,kk)*x1(indice)
                  enddo
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
            endif

#ifdef USE_FFTW
            call dfftw_execute_dft(planf,b11,b11)
#else
            call fftsingletonz2d(b11,NX2,NY2,FFTW_FORWARD)
#endif

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indice,ii) 
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)     
            do j=1,ny
               do i=1,nx
                  indice=i+nx2*(j-1)           
                  ii=i+nx*((j-1)+ny*(ll-1))
                  FF(ii)=FF(ii)+b11(indice)
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
         enddo
      enddo

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,jj)    
!$OMP DO SCHEDULE(STATIC)
      do i=1,ndipole
         if (Tabdip(i).ne.0) then
            jj=Tabdip(i)
            xr(jj)=xr(jj)-FF(i)/dntotal
         endif
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
      return
      end
