c     FFB=FFB-A*FFX==> FFB=XI et FFX=XI*pola==> FFB=(I-A*pola)*XI
c     routine optimiser pour le cube.
      subroutine produitfftmatvectoptscalaire(FFTTENSORxx,vectx,ntotalm
     $     ,ntotal,nmax,nxm,nym,nzm,nx,ny,nz,nx2,ny2,nxy2,nz2,FFX,FFB
     $     ,planf,planb)
      implicit none
      integer jj,i,j,k,indice,ntotal,ntotalm,nxm,nym,nzm,nx,ny,nz
     $     ,nmax,nx2,ny2,nxy2,nz2
      double complex FFTTENSORxx(ntotalm),FFX(3*nmax),FFB(3 *nmax)
     $     ,vectx(ntotalm) 
      double complex ctmpx
      integer nxy
      double precision dntotal
      integer*8 planf,planb
c      double precision t1,t2
      integer FFTW_BACKWARD, FFTW_FORWARD 

      FFTW_BACKWARD=+1
      FFTW_FORWARD=-1

      nxy=nx*ny
      dntotal=dble(ntotal)
      
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(indice) 
!$OMP DO SCHEDULE(STATIC)      
      do indice=1,ntotal
         vectx(indice)=0.d0
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(jj,i,j,k,indice) 
!$OMP DO SCHEDULE(STATIC) COLLAPSE(3)       
      do k=1,nz
         do j=1,ny
            do i=1,nx
c     position du dipole
               indice=i+nx2*(j-1)+nxy2*(k-1)
               jj=i+nx*(j-1)+nxy*(k-1)
               vectx(indice)=FFX(jj)
            enddo
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

#ifdef USE_FFTW
      call dfftw_execute_dft(planb, vectx, vectx)   
#else
      call fftsingletonz3d(vectx,NX2,NY2,NZ2,FFTW_BACKWARD)
#endif

      
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(indice,ctmpx)   
!$OMP DO SCHEDULE(STATIC)   
      do indice=1,ntotal
         ctmpx=vectx(indice)
         vectx(indice)=FFTTENSORxx(indice)*ctmpx     
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      
c     FFT inverse (deconvolution)
#ifdef USE_FFTW
      call dfftw_execute_dft(planf, vectx, vectx)
#else
      call fftsingletonz3d(vectx,NX2,NY2,NZ2,FFTW_FORWARD)
#endif
      
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(indice,jj,i,j,k)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(3)               
      do k=1,nz
         do j=1,ny
            do i=1,nx
               indice=i+nx2*(j-1)+nxy2*(k-1)
               jj=i+nx*(j-1)+nxy*(k-1)
               FFB(jj)=FFB(jj)-vectx(indice)/dntotal
            enddo
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      end
   

c*************************************************
